# babel-plugin-native-async-for-typescript

## Note
TypeScript has added support for skipping transpiling async functions to generators. If you were a user of this plugin, consider upgrading your TypeScript version instead.

## Example
<table>
<tr><th>In</th><th>Out</th></tr>
<tr><td>
<pre lang="javascript">
// Compiled by TypeScript
function printDelayed(nodes) {
  return __awaiter(this, void 0, void 0, function* () {
    for (var _i = 0; _i < nodes.length; _i++) {
      var node = nodes[_i];
      yield delay(200);
      console.log(node);
    }
  });
}
</pre>
</td>
<td>
<pre lang="javascript">
// Compiled with this Babel plugin.
async function printDelayed(nodes) {
  for (var _i = 0; _i < nodes.length; _i++) {       
    var node = nodes[_i];
    await delay(200);
    console.log(node);
  }
}
</pre>
</td>
</tr>
</table>

## Installation

```sh
$ npm install babel-plugin-native-async-for-typescript
```

## Usage

### Via `.babelrc` (Recommended)

**.babelrc**

```json
{
  "plugins": ["native-async-for-typescript"]
}
```

### Via CLI

```sh
$ babel --plugins native-async-for-typescript script.js
```

### Via Node API

```javascript
require("babel-core").transform("code", {
  plugins: ["native-async-for-typescript"]
});
```
