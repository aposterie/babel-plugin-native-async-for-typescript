import * as t from 'babel-types';
import { anyPass as either, last } from 'ramda';
import { Node, NodePath, Visitor } from 'babel-traverse';

export default function babelPluginNativeAsyncForTypeScript() {
	return { visitor };
}

function getParents<T>( path: NodePath<T>, n = Infinity ): Array<typeof path.parentPath> {
	const parents = [];
	while ( n-- ) {
		if ( path ) {
			path = path.parentPath as any;
			parents.push( path );
		} else if ( n === Infinity ) {
			break;
		} else {
			parents.push( {} );
		}
	}
	return parents;
}

function isAwaiterCallExpression( node: t.CallExpression ) {
	return t.isIdentifier( node.callee, { name: '__awaiter' } );
}

function isGeneratorFunction( node ): node is t.Function {
	return t.isFunction( node, { generator: true } );
}

namespace visitor {

	namespace yieldToAwait {
		export function YieldExpression( path: NodePath<t.YieldExpression> ) {
			const { node } = path;
			if ( node.delegate ) throw new Error('Cannot transform delegated yield expression to await');
			path.replaceWith( t.awaitExpression( node.argument ) );
		}
	}

	export function CallExpression( path: NodePath<t.CallExpression> ) {
		const { node } = path;
		const parents = getParents( path, 4 );
		const gen: t.Function | t.FunctionDeclaration = <any> last( node.arguments );

		if ( isAwaiterCallExpression( node )          // [[__awaiter]](...
			&& t.isThisExpression( node.arguments[0] )  // __awaiter([[this]], ...)
			&& isGeneratorFunction( gen )               // __awaiter(..., [[function *]])
			&& either([
					t.isReturnStatement,                    // [[return]] __awaiter...
					t.isArrowFunctionExpression             // [[() =>]] __awaiter...
				])( parents[0].node )
		) {

			const func = t.isBlockStatement( parents[1].node ) ? parents[2]
				: t.isArrowFunctionExpression( parents[0].node ) ? parents[0]
				: parents[1];
			const node = func.node as t.Function;

			switch (node.type) {

				case 'FunctionDeclaration':
					path.traverse( yieldToAwait );
					func.replaceWith(
						t.functionDeclaration( node.id, node.params, (gen as t.FunctionDeclaration).body, false, true )
					);
					break;

				case 'FunctionExpression':
					path.traverse( yieldToAwait );
					func.replaceWith(
						t.functionExpression( node.id, node.params, (gen as t.FunctionExpression).body, false, true )
					);
					break;

				case 'ArrowFunctionExpression':
					path.traverse( yieldToAwait );
					func.replaceWith(
						t.arrowFunctionExpression( node.params, gen.body, true )
					);
					break;
			}
		}
	}
}
