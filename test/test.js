const path = require('path');
const { name } = require('../package.json');

// babel-helper-transform-fixture-test-runner hard codes the "babel-plugin-"
// into its resolving algorithm, which works just as well as you expect.

const resolver = require('babel-core/lib/helpers/resolve');
resolver.__esModule = true;
resolver.default = function ( loc, relative ) {
	if ( loc.endsWith( name ) ) {
		return path.resolve( '.' );
	} else {
		return resolver( loc, relative );
	}
};

require('babel-helper-plugin-test-runner')(__dirname);
