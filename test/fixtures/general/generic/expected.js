async function printDelayed(elements) {
  for (var _i = 0, elements_1 = elements; _i < elements_1.length; _i++) {
    var element = elements_1[_i];
    await delay(200);
    console.log(element);
  }
}
