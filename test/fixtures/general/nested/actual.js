function printDelayed(elements) {
  return __awaiter(this, void 0, void 0, function* () {
    for (var _i = 0, elements_1 = elements; _i < elements_1.length; _i++) {
      var element = elements_1[_i];
      yield delay(200);
      var printer = function () {
          return __awaiter(this, void 0, void 0, function* () {
              console.log(element);
          });
      };
      printer();
    }
  });
}
