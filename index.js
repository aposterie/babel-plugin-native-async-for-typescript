"use strict";
var t = require('babel-types');
var ramda_1 = require('ramda');
function babelPluginNativeAsyncForTypeScript() {
    return { visitor: visitor };
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = babelPluginNativeAsyncForTypeScript;
function getParents(path, n) {
    if (n === void 0) { n = Infinity; }
    var parents = [];
    while (n--) {
        if (path) {
            path = path.parentPath;
            parents.push(path);
        }
        else if (n === Infinity) {
            break;
        }
        else {
            parents.push({});
        }
    }
    return parents;
}
function isAwaiterCallExpression(node) {
    return t.isIdentifier(node.callee, { name: '__awaiter' });
}
function isGeneratorFunction(node) {
    return t.isFunction(node, { generator: true });
}
var visitor;
(function (visitor) {
    var yieldToAwait;
    (function (yieldToAwait) {
        function YieldExpression(path) {
            var node = path.node;
            if (node.delegate)
                throw new Error('Cannot transform delegated yield expression to await');
            path.replaceWith(t.awaitExpression(node.argument));
        }
        yieldToAwait.YieldExpression = YieldExpression;
    })(yieldToAwait || (yieldToAwait = {}));
    function CallExpression(path) {
        var node = path.node;
        var parents = getParents(path, 4);
        var gen = ramda_1.last(node.arguments);
        if (isAwaiterCallExpression(node)
            && t.isThisExpression(node.arguments[0])
            && isGeneratorFunction(gen)
            && ramda_1.anyPass([
                t.isReturnStatement,
                t.isArrowFunctionExpression
            ])(parents[0].node)) {
            var func = t.isBlockStatement(parents[1].node) ? parents[2]
                : t.isArrowFunctionExpression(parents[0].node) ? parents[0]
                    : parents[1];
            var node_1 = func.node;
            switch (node_1.type) {
                case 'FunctionDeclaration':
                    path.traverse(yieldToAwait);
                    func.replaceWith(t.functionDeclaration(node_1.id, node_1.params, gen.body, false, true));
                    break;
                case 'FunctionExpression':
                    path.traverse(yieldToAwait);
                    func.replaceWith(t.functionExpression(node_1.id, node_1.params, gen.body, false, true));
                    break;
                case 'ArrowFunctionExpression':
                    path.traverse(yieldToAwait);
                    func.replaceWith(t.arrowFunctionExpression(node_1.params, gen.body, true));
                    break;
            }
        }
    }
    visitor.CallExpression = CallExpression;
})(visitor || (visitor = {}));
//# sourceMappingURL=index.js.map